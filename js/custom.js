// partners slider
$('.ourpartners').slick({
  dots: false,
  infinite: false,
  arrows: false,
  speed: 300,
  slidesToShow: 5,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});


// testimonal slider
$('.clientreview').slick({
  dots: true,
  arrows: false,
  speed: 300,
  slidesToShow: 3,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000,
  infinite:true,
  cssEase: 'linear',
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});


// tour package
 $('.tourpackage-slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  dots:false,
  arrows: false,
  autoplay: true,
  autoplaySpeed: 2000,
  fade: true,
  asNavFor: '.tourpackage-nav'
});

$('.tourpackage-nav').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000,
  asNavFor: '.tourpackage-slider',
  dots: true,
  centerMode: true,
  focusOnSelect: true
});


$('.banner-slider').slick({
  slidesToScroll: 1,
  infinite: true,
  autoplay: true,
  autoplaySpeed: 2400,
  fade: true,
  infinite:true,
  dots:true,
  arrows:true,
  cssEase: 'linear',
});
  

// back to top
$(document).ready(function($){
    $(window).scroll(function(){
        
        if ($(this).scrollTop() > 600) {
            $('.backtotop a').css("display","block");
        } else {
            $('.backtotop a').css("display","none");
        }
    });
    $('.backtotop a').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 500);
        return false;
    });
});



// 
$(document).ready(function($){
    $(window).scroll(function(){
        
        if ($(this).scrollTop() > 600) {
            $('.fixedmap').css("display","inline-grid");
        } else {
            $('.fixedmap').css("display","none");
        }
    });
});

// Tweets
$('.tweetsreview').slick({
  dots: true,
  arrows: false,
  speed: 300,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    },
  ]
});

// cursor pointer
$(document).ready(function() {
  $(".search-popup").mousemove(function(event){            
    var relX = event.pageX - $(this).offset().left;
    var relY = event.pageY - $(this).offset().top;
    var relBoxCoords = "(" + relX + "," + relY + ")";
    $('.pointer-tranform').css('left',relX);
    $('.pointer-tranform').css('top',relY);
  });

  $("#searchbtn-open").click(function(){
    $('#searchbtn').css('display','block');
  });


  $("#searchbtn").click(function(){
    $(this).css('display','none');
  });

  $( ".search-body" ).click(function( event ) {
    event.stopPropagation();
  });

});


// fixed navbar 
$(window).scroll(function() {    
  var scroll = $(window).scrollTop();
         
  if (scroll >= 3) {
     $(".navigation").addClass("fixed-nav");
  }
  else{
    $(".navigation").removeClass("fixed-nav");
     }
}); 


$(document).ready(function(){
var t = $(document).width();
    if (t < 768) {
       $('.smhide-collapse').removeClass('show');
    } else {
       $('.smhide-collapse').addClass('show');
   }
});

// price slider
var limitSlider = document.getElementById('rangeslider');

noUiSlider.create(limitSlider, {
    start: [30, 150],
    behaviour: 'drag',
    connect: true,
    range: {
        'min': 0,
        'max': 200
    }
});

var limitFieldMin = document.getElementById('range-slider-value-min');
var limitFieldMax = document.getElementById('range-slider-value-max');

limitSlider.noUiSlider.on('update', function (values, handle) {
    (handle ? limitFieldMax : limitFieldMin).innerHTML = values[handle];
});

// counter js
$('.counter').counterUp({
    delay: 10,
    time: 1000
});
